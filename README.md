# 关于低代码平台,基本上不再维护,有问题自己研究,也可以付费咨询,具体请看官网介绍

# jqp-admin低代码开发平台

## 所有代码全部开源

## 在线演示

https://www.jqp-admin.com
账号 admin
密码 1

登录后选择企业2,企业2目前配置了全量的权限

## 官网地址

https://jqp-admin.com

## 数据库以及markdown文档地址

https://gitee.com/hyz79/jqp-doc

## 前后端不分离版本(兼容分离和不分离版本)

https://gitee.com/hyz79/jqp-admin

## 前后端分离版本-前端

https://gitee.com/hyz79/jqp-admin-page

## 前后端分离版本-后端 (此项目长时间未更新,废弃,jqp-admin项目兼容分离和不分离版本)

https://gitee.com/hyz79/jqp-admin-rest
